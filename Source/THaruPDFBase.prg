/*
 * Proyecto: QuickSQL
 * Fichero: THaruPDFBase.prg
 * Descripci�n:
 * Autor: Carlos Mora
 * Fecha: 26/03/2013
 */

#include 'hbclass.ch'
#include 'common.ch'
#include 'harupdf.ch'

// #define __NODEBUG__
#include 'debug.ch'

#ifdef __XHARBOUR__
#translate HB_HHasKey( <u> ) => HHasKey( <u> )
#endif

//------------------------------------------------------------------------------
CLASS THaruPDFBase

   // Implementaci�n de la clase wrapper, que reemplaza al objeto Printer de FW
   // No se usa directamente sino a trav�s de la
   DATA hPdf
   DATA hPage
   DATA LoadedFonts

   DATA nOrientation INIT HPDF_PAGE_PORTRAIT // HPDF_PAGE_LANDSCAPE
   DATA nHeight  INIT 21   *72/2.54
   DATA nWidth   INIT 29.7 *72/2.54

   DATA cFileName
   DATA nPermission
   DATA cPassword, cOwnerPassword

   DATA hImageList

   DATA lPreview INIT .F.
   DATA bPreview

   CONSTRUCTOR New()
   METHOD SetPage()
   METHOD SetLandscape()
   METHOD SetPortrait()
   METHOD SetCompression( cMode ) INLINE HPDF_SetCompressionMode( ::hPdf, cMode )
   METHOD StartPage()
   METHOD EndPage()
   METHOD Say()
   METHOD CmSay()
   METHOD SayRotate( nTop, nLeft, cTxt, oFont, nClrText, nAngle )
   METHOD DefineFont()
   METHOD Cmtr2Pix( nRow, nCol )
   METHOD Mmtr2Pix( nRow, nCol )
   METHOD CmRect2Pix()
   METHOD nVertRes() INLINE 72
   METHOD nHorzRes() INLINE 72
   METHOD nLogPixelX() INLINE 72  // Number of pixels per logical inch
   METHOD nLogPixelY() INLINE 72
   METHOD nVertSize() INLINE HPDF_Page_GetHeight( ::hPage )
   METHOD nHorzSize() INLINE HPDF_Page_GetWidth( ::hPage )
   METHOD SizeInch2Pix()
   METHOD CmSayBitmap()
   METHOD SayBitmap()
   METHOD GetImageFromFile()
   METHOD Line()
   METHOD CmLine()
   METHOD Rect()
   METHOD CmRect()
   MESSAGE BOX METHOD Rect
   MESSAGE CmBox METHOD CmRect
   METHOD RoundBox( nTop, nLeft, nBottom, nRight, nWidth, nHeight, oPen, nColor, nBackColor, lFondo )
   METHOD CmRoundBox( nTop, nLeft, nBottom, nRight, nWidth, nHeight, oPen, nColor, nBackColor, lFondo )
   MESSAGE RoundRect METHOD RoundBox
   MESSAGE CmRoundRect METHOD CmRoundBox
   METHOD SetPen()
   METHOD DashLine()
   METHOD CmDashLine()
   METHOD SAVE()
   METHOD SyncPage()
   METHOD CheckPage()
   METHOD GetTextWidth()
   METHOD GetTextHeight()
   METHOD END()

ENDCLASS

//------------------------------------------------------------------------------
METHOD New( cFileName, cPassword, cOwnerPassword, nPermission, lPreview, bPreview )

   //------------------------------------------------------------------------------
   ::hPdf := HPDF_New()
   ::LoadedFonts := {}

   IF ::hPdf == NIL
      Alert( " Pdf could not been created!" )
      RETURN NIL
   ENDIF

   HPDF_SetCompressionMode( ::hPdf, HPDF_COMP_ALL )

   ::cFileName := cFileName
   ::cPassword := cPassword
   ::cOwnerPassword := cOwnerPassword
   ::nPermission := nPermission

   ::hImageList := { => }

   // Mastintin
   IF HB_ISLOGICAL( lPreview )
      ::lPreview := lPreview
      IF HB_ISBLOCK( bPreview )
         ::bPreview := bPreview
      ELSE
         ::bPreview := {|| HaruOpenPdf( ::cFileName ) }
      ENDIF
   ENDIF

RETURN Self

//------------------------------------------------------------------------------
METHOD SetPage( nPageSize )
   static aFW2HARUSizes
   if empty(aFW2HARUSizes)
      aFW2HARUSizes := {}
      aAdd(aFW2HARUSizes, { 8.5 *72     ,11   *72     }) //#define DMPAPER_LETTER    1       // Letter 8 1/2 x 11 in
      aAdd(aFW2HARUSizes, { 8.5 *72     ,11   *72     }) //#define DMPAPER_LETTERSMALL 2       // Letter Small 8 1/2 x 11 in
      aAdd(aFW2HARUSizes, {11   *72     ,17   *72     }) //#define DMPAPER_TABLOID   3       // Tabloid 11 x 17 in
      aAdd(aFW2HARUSizes, {11   *72     ,17   *72     }) ////#define DMPAPER_LEDGER    4       // Ledger 17 x 11 in
      aAdd(aFW2HARUSizes, { 8.5 *72     ,14   *72     }) //#define DMPAPER_LEGAL     5       // Legal 8 1/2 x 14 in
      aAdd(aFW2HARUSizes, { 5.5 *72     , 8.5 *72     }) //#define DMPAPER_STATEMENT   6       // Statement 5 1/2 x 8 1/2 in
      aAdd(aFW2HARUSizes, { 7.25*72     ,10.5 *72     }) //#define DMPAPER_EXECUTIVE   7       // Executive 7 1/4 x 10 1/2 in
      aAdd(aFW2HARUSizes, {29.7 *72/2.54,42   *72/2.54}) //#define DMPAPER_A3      8       // A3 297 x 420 mm
      aAdd(aFW2HARUSizes, {21   *72/2.54,29.7 *72/2.54}) //#define DMPAPER_A4      9       // A4 210 x 297 mm
      aAdd(aFW2HARUSizes, {21   *72/2.54,29.7 *72/2.54}) //#define DMPAPER_A4SMALL   10      // A4 Small 210 x 297 mm
      aAdd(aFW2HARUSizes, {14.8 *72/2.54,21   *72/2.54}) //#define DMPAPER_A5      11      // A5 148 x 210 mm
      aAdd(aFW2HARUSizes, {25   *72/2.54,35.4 *72/2.54}) //#define DMPAPER_B4      12      // B4 250 x 354
      aAdd(aFW2HARUSizes, {18.2 *72/2.54,25.7 *72/2.54}) //#define DMPAPER_B5      13      // B5 182 x 257 mm
      aAdd(aFW2HARUSizes, { 8.5 *72     ,13   *72     }) //#define DMPAPER_FOLIO     14      // Folio 8 1/2 x 13 in
      aAdd(aFW2HARUSizes, {25.5 *72/2.54,27.5 *72/2.54}) //#define DMPAPER_QUARTO    15      // Quarto 215 x 275 mm
      aAdd(aFW2HARUSizes, {10   *72     ,14   *72     }) //#define DMPAPER_10X14     16      // 10x14 in
      aAdd(aFW2HARUSizes, {11   *72     ,17   *72     }) //#define DMPAPER_11X17     17      // 11x17 in
      aAdd(aFW2HARUSizes, { 8.5 *72     ,11   *72     }) //#define DMPAPER_NOTE      18      // Note 8 1/2 x 11 in
      aAdd(aFW2HARUSizes, {3.875*72     ,8.875*72     }) //#define DMPAPER_ENV_9     19      // Envelope #9 3 7/8 x 8 7/8
      aAdd(aFW2HARUSizes, {4.125*72     , 9.5 *72     }) //#define DMPAPER_ENV_10    20      // Envelope #10 4 1/8 x 9 1/2
      aAdd(aFW2HARUSizes, { 4.5 *72     ,10.375*72    }) //#define DMPAPER_ENV_11    21      // Envelope #11 4 1/2 x 10 3/8
      aAdd(aFW2HARUSizes, { 4.75*72     ,11   *72     }) //#define DMPAPER_ENV_12    22      // Envelope #12 4 3\4 x 11
      aAdd(aFW2HARUSizes, { 5   *72     ,11.5 *72     }) //#define DMPAPER_ENV_14    23      // Envelope #14 5 x 11 1/2
      aAdd(aFW2HARUSizes, {17   *72     ,22   *72     }) //#define DMPAPER_CSHEET    24      // C size sheet 17 x 22-inches
      aAdd(aFW2HARUSizes, {22   *72     ,34   *72     }) //#define DMPAPER_DSHEET    25      // D size sheet 22- by 34-inches
      aAdd(aFW2HARUSizes, {34   *72     ,44   *72     }) //#define DMPAPER_ESHEET    26      // E size sheet 34x44
      aAdd(aFW2HARUSizes, {11   *72/2.54,22   *72/2.54}) //#define DMPAPER_ENV_DL    27      // Envelope DL 110 x 220mm
      aAdd(aFW2HARUSizes, {16.2 *72/2.54,22.9 *72/2.54}) //#define DMPAPER_ENV_C5    28      // Envelope C5 162 x 229 mm
      aAdd(aFW2HARUSizes, {23.4 *72/2.54,45.8 *72/2.54}) //#define DMPAPER_ENV_C3    29      // Envelope C3  324 x 458 mm
      aAdd(aFW2HARUSizes, {22.9 *72/2.54,32.4 *72/2.54}) //#define DMPAPER_ENV_C4    30      // Envelope C4  229 x 324 mm
      aAdd(aFW2HARUSizes, {11.4 *72/2.54,16.2 *72/2.54}) //#define DMPAPER_ENV_C6    31      // Envelope C6  114 x 162 mm
      aAdd(aFW2HARUSizes, {11.4 *72/2.54,22.9 *72/2.54}) //#define DMPAPER_ENV_C65   32      // Envelope C65 114 x 229 mm
      aAdd(aFW2HARUSizes, {25   *72/2.54,25.3 *72/2.54}) //#define DMPAPER_ENV_B4    33      // Envelope B4  250 x 353 mm
      aAdd(aFW2HARUSizes, {17.6 *72/2.54,25   *72/2.54}) //#define DMPAPER_ENV_B5    34      // Envelope B5  176 x 250 mm
      aAdd(aFW2HARUSizes, {17.6 *72/2.54,12.5 *72/2.54}) //#define DMPAPER_ENV_B6    35      // Envelope B6  176 x 125 mm
      aAdd(aFW2HARUSizes, {11   *72/2.54,23   *72/2.54}) //#define DMPAPER_ENV_ITALY   36      // Envelope 110 x 230 mm
      aAdd(aFW2HARUSizes, {3.875*72     , 7.5 *72     }) //#define DMPAPER_ENV_MONARCH 37      // Envelope Monarch 3.875 x 7.5 in
      aAdd(aFW2HARUSizes, {3.625*72     , 6.5 *72     }) //#define DMPAPER_ENV_PERSONAL 38       // 6 3/4 Envelope 3 5/8 x 6 1/2 in
      aAdd(aFW2HARUSizes, {14.875*72    ,11   *72     }) //#define DMPAPER_FANFOLD_US  39      // US Std Fanfold 14 7/8 x 11 in
      aAdd(aFW2HARUSizes, { 8.5 *72     ,12   *72     }) //#define DMPAPER_FANFOLD_STD_GERMAN  40  // German Std Fanfold 8 1/2 x 12 in
      aAdd(aFW2HARUSizes, { 8.5 *72     ,13   *72     }) //#define DMPAPER_FANFOLD_LGL_GERMAN  41  // German Legal Fanfold 8 1/2 x 13 in
    endif
   //------------------------------------------------------------------------------
   ::nWidth := aFW2HARUSizes[nPageSize,1]
   ::nHeight := aFW2HARUSizes[nPageSize,2]
   ::SyncPage()

RETURN Self

//------------------------------------------------------------------------------
METHOD SyncPage()
    LOCAL tmp
   //------------------------------------------------------------------------------
   IF ::hPage != NIL
      if ::nOrientation = HPDF_PAGE_LANDSCAPE .and. ::nWidth<::nHeight
         tmp := ::nWidth
         ::nWidth := ::nHeight
         ::nHeight := tmp
      endif
      if ::nOrientation = HPDF_PAGE_PORTRAIT .and. ::nWidth>::nHeight
         tmp := ::nWidth
         ::nWidth := ::nHeight
         ::nHeight := tmp
      endif
      //WQOut(::hPage, ::nWidth, ::nHeight)
      HPDF_Page_SetWidth( ::hPage, ::nWidth)
      HPDF_Page_SetHeight( ::hPage, ::nHeight)
   ENDIF

RETURN NIL

//------------------------------------------------------------------------------
METHOD CheckPage()

   //------------------------------------------------------------------------------
   IF ::hPage == NIL
      ::StartPage()
   ENDIF

RETURN NIL

//------------------------------------------------------------------------------
METHOD SetLandscape()

   //------------------------------------------------------------------------------
   ::nOrientation := HPDF_PAGE_LANDSCAPE
   ::SyncPage()

RETURN Self

//------------------------------------------------------------------------------
METHOD SetPortrait()

   //------------------------------------------------------------------------------
   ::nOrientation := HPDF_PAGE_PORTRAIT
   ::SyncPage()

RETURN Self

//------------------------------------------------------------------------------
METHOD StartPage()

   //------------------------------------------------------------------------------
   ::hPage := HPDF_AddPage( ::hPdf )
   ::SyncPage()

RETURN Self

//------------------------------------------------------------------------------
METHOD EndPage()

   //------------------------------------------------------------------------------
   ::hPage := NIL

RETURN Self

//------------------------------------------------------------------------------
METHOD Say( nRow, nCol, cText, oFont, nWidth, nClrText, nBkMode, nPad )

   //------------------------------------------------------------------------------
   LOCAL c, nTextHeight

   ::CheckPage()
   HPDF_Page_BeginText( ::hPage )
   IF oFont == NIL
      nTextHeight := HPDF_Page_GetCurrentFontSize( ::hPage )
   ELSE
      HPDF_Page_SetFontAndSize( ::hPage, oFont[ 1 ], oFont[ 2 ] )
      nTextHeight := oFont[ 2 ]
   ENDIF
   IF ValType( nClrText ) == 'N'
      c := HPDF_Page_GetRGBFill( ::hPage )
      HPDF_Page_SetRGBFill( ::hPage, ( Int( nClrText / 0x10000 ) % 256 ) / 256.00, ( Int( nClrText / 0x100 )  % 256 )  / 256.00, ( nClrText  % 256 ) / 256.00 )
   ENDIF

   DO CASE
   CASE nPad == NIL .OR. nPad == HPDF_TALIGN_LEFT
      HPDF_Page_TextOut( ::hPage, nCol, ::nHeight - nRow - nTextHeight, cText )
   CASE nPad == HPDF_TALIGN_RIGHT
      nWidth := HPDF_Page_TextWidth( ::hPage, cText )
      HPDF_Page_TextOut( ::hPage, nCol - nWidth, ::nHeight - nRow - nTextHeight, cText )
      OTHER
      nWidth := HPDF_Page_TextWidth( ::hPage, cText )
      HPDF_Page_TextOut( ::hPage, nCol - nWidth / 2, ::nHeight - nRow - nTextHeight, cText )
   ENDCASE

   IF ValType( c ) == 'A'
      HPDF_Page_SetRGBFill( ::hPage, c[ 1 ], c[ 2 ], c[ 3 ] )
   ENDIF
   HPDF_Page_EndText( ::hPage )

RETURN Self

//------------------------------------------------------------------------------
METHOD CmSay( nRow, nCol, cText, oFont, nWidth, nClrText, nBkMode, nPad, lO2A )

   //------------------------------------------------------------------------------

   ::Cmtr2Pix( @nRow, @nCol )
   IF  nWidth != Nil
      ::Cmtr2Pix( 0, @nWidth )
   ENDIF
   ::Say( nRow, nCol, cText, oFont, nWidth, nClrText, nBkMode, nPad, lO2A )

RETURN Self


//------------------------------------------------------------------------------
METHOD DefineFont( cFontName, nSize, lEmbed )

   //------------------------------------------------------------------------------
   LOCAL font_list  := { ;
      "Courier",                  ;
      "Courier-Bold",             ;
      "Courier-Oblique",          ;
      "Courier-BoldOblique",      ;
      "Helvetica",                ;
      "Helvetica-Bold",           ;
      "Helvetica-Oblique",        ;
      "Helvetica-BoldOblique",    ;
      "Times-Roman",              ;
      "Times-Bold",               ;
      "Times-Italic",             ;
      "Times-BoldItalic",         ;
      "Symbol",                   ;
      "ZapfDingbats"              ;
      }

   LOCAL i, ttf_list

   i := AScan( font_list, {| x| Upper( x ) == Upper( cFontName ) } )
   IF i > 0 // Standard font
      cFontName := font_list[ i ]
   ELSE
      i := AScan( ::LoadedFonts, {| x| Upper( cFontName ) $ Upper( x[ 1 ] ) } )
      IF i > 0
         cFontName := ::LoadedFonts[ i ][ 2 ]
         DEBUGMSG 'Activada fuente ' + cFontName
      ELSE
         ttf_list := GetHaruFontList()
         i := AScan( ttf_list, {| x| Upper( cFontName ) $ Upper( x[ 1 ] ) } )
         IF i > 0
            cFontName := HPDF_LoadTTFontFromFile( ::hPdf, ttf_list[ i, 2 ], lEmbed )
            DEBUGMSG 'Cargada fuente ' + cFontName
            DEBUGMSG 'Fichero ' + ttf_list[ i, 2 ]
            AAdd( ::LoadedFonts, { ttf_list[ i, 1 ], cFontName } )
         ELSE
            Alert( 'Fuente desconocida ' + cFontName )
            RETURN NIL
         ENDIF
      ENDIF
   ENDIF

RETURN { HPDF_GetFont( ::hPdf, cFontName, "WinAnsiEncoding" ), nSize }

METHOD Cmtr2Pix( nRow, nCol )

   nRow *= 72 / 2.54
   nCol *= 72 / 2.54

RETURN {nRow,nCol}

METHOD Mmtr2Pix( nRow, nCol )

   nRow *= 72 / 25.4
   nCol *= 72 / 25.4

RETURN {nRow,nCol}

METHOD CmRect2Pix( aRect )

   LOCAL aTmp[ 4 ]

   aTmp[ 1 ] = Max( 0, aRect[ 1 ] * 72 / 2.54 )
   aTmp[ 2 ] = Max( 0, aRect[ 2 ] * 72 / 2.54 )
   aTmp[ 3 ] = Max( 0, aRect[ 3 ] * 72 / 2.54 )
   aTmp[ 4 ] = Max( 0, aRect[ 4 ] * 72 / 2.54 )

RETURN aTmp

METHOD SizeInch2Pix( nHeight, nWidth )

   nHeight *= 72
   nWidth *= 72

RETURN { nHeight, nWidth }

METHOD GetImageFromFile( cImageFile )

   IF hb_HHasKey( ::hImageList, cImageFile )
      RETURN ::hImageList[ cImageFile ]
   ENDIF
   IF !File( cImageFile )
      IF( Lower( Right( cImageFile, 4 ) ) == '.bmp' ) // En el c�digo esta como bmp, probar si ya fue transformado a png
         cImageFile := Left( cImageFile, Len( cImageFile ) - 3 ) + 'png'
         RETURN ::GetImageFromFile( cImageFile )
      ELSE
         Alert( cImageFile + ' no encontrado' )
         RETURN NIL
      ENDIF
   ENDIF
   IF( Lower( Right( cImageFile, 4 ) ) == '.png' )
      RETURN ( ::hImageList[ cImageFile ] := HPDF_LoadPngImageFromFile( ::hPdf, cImageFile ) )
   ENDIF

RETURN ( ::hImageList[ cImageFile ] := HPDF_LoadJpegImageFromFile( ::hPdf, cImageFile ) )

METHOD SayBitmap( nRow, nCol, xBitmap, nWidth, nHeight, nRaster )

   LOCAL image

   IF !Empty( image := ::GetImageFromFile( xBitmap ) )
      HPDF_Page_DrawImage( ::hPage, image, nCol, ::nHeight - nRow - nHeight, nWidth, nHeight /* iw, ih*/)
   ENDIF

RETURN Self

METHOD Line( nTop, nLeft, nBottom, nRight, oPen )

   IF oPen != NIL
      ::SetPen( oPen )
   ENDIF
   HPDF_Page_MoveTo ( ::hPage, nLeft, ::nHeight - nTop )
   HPDF_Page_LineTo ( ::hPage, nRight, ::nHeight - nBottom )
   HPDF_Page_Stroke ( ::hPage )

RETURN Self

METHOD SAVE( cFilename )

   FErase( cFilename )

   IF ValType( ::nPermission ) != 'N'
      ::nPermission := ( HPDF_ENABLE_READ + HPDF_ENABLE_PRINT + HPDF_ENABLE_COPY )
   ENDIF

   IF ValType( ::cPassword ) == 'C' .AND. !Empty( ::cPassword )
      IF Empty( ::cOwnerPassword )
         ::cOwnerPassword := ::cPassword + '+1'
      ENDIF
      HPDF_SetPassword( ::hPdf, ::cOwnerPassword, ::cPassword )
      HPDF_SetPermission( ::hPdf, ::nPermission )
   ENDIF

RETURN HPDF_SaveToFile ( ::hPdf, cFilename )

METHOD GetTextWidth( cText, oFont )

   HPDF_Page_SetFontAndSize( ::hPage, oFont[ 1 ], oFont[ 2 ] )

RETURN HPDF_Page_TextWidth( ::hPage, cText )

METHOD GetTextHeight( cText, oFont )

   HPDF_Page_SetFontAndSize( ::hPage, oFont[ 1 ], oFont[ 2 ] )

RETURN oFont[ 2 ] // La altura de la fuente cuando la creamos

METHOD END()

   LOCAL nResult

   IF ValType( ::cFileName ) == 'C'
      nResult := ::Save( ::cFileName )
   ENDIF

   HPDF_Free( ::hPdf )

   IF ::lPreview
      Eval( ::bPreview, Self )
   ENDIF

RETURN nResult

METHOD Rect( nTop, nLeft, nBottom, nRight, oPen, nColor )

   HPDF_Page_GSave(::hPage)

   ::SetPen( oPen, nColor )

   HPDF_Page_Rectangle( ::hPage, nLeft, ::nHeight - nBottom, nRight - nLeft,  nBottom - nTop )

   HPDF_Page_GRestore(::hPage)

RETURN Self


METHOD CmRect( nTop, nLeft, nBottom, nRight, oPen, nColor )

   ::Rect( nTop * 72 / 2.54, nLeft * 72 / 2.54, nBottom * 72 / 2.54, nRight * 72 / 2.54, oPen, nColor )

RETURN Self

METHOD CmLine( nTop, nLeft, nBottom, nRight, oPen )

   ::Line( nTop * 72 / 2.54, nLeft * 72 / 2.54, nBottom * 72 / 2.54, nRight * 72 / 2.54, oPen )

RETURN Self

METHOD CmDashLine( nTop, nLeft, nBottom, nRight, oPen, nDashMode )

   ::DashLine( nTop * 72 / 2.54, nLeft * 72 / 2.54, nBottom * 72 / 2.54, nRight * 72 / 2.54, oPen, nDashMode )

RETURN Self

METHOD DashLine( nTop, nLeft, nBottom, nRight, oPen, nDashMode )

   HPDF_Page_SetDash( ::hPage, { 3, 7 }, 2, 2 )
   ::Line( nTop, nLeft, nBottom, nRight, oPen )
   HPDF_Page_SetDash( ::hPage, NIL, 0, 0 )

RETURN Self

METHOD CmSayBitmap( nRow, nCol, xBitmap, nWidth, nHeight, nRaster )
RETURN ::SayBitmap( nRow * 72 / 2.54, nCol * 72 / 2.54, xBitmap, nWidth * 72 / 2.54, nHeight * 72 / 2.54, nRaster )

// METHOD Rect( nTop, nLeft, nBottom, nRight, oPen, nColor , nRound , nBackColor, lFondo )
METHOD RoundBox( nTop, nLeft, nBottom, nRight, nWidth, nHeight, oPen, nColor, nBackColor )

   LOCAL nRay
   LOCAL xposTop, xposBotton
   LOCAL nRound

   DEFAULT nWidth To 0, nHeight TO 0

   nRound:= Min( nWidth, nHeight )

   HPDF_Page_GSave(::hPage)
   ::SetPen( oPen, nColor )

   IF HB_ISNUMERIC( nBackColor )
      HPDF_Page_SetRGBFill( ::hPage, ( nBackColor  % 256 ) / 256.00, ( Int( nBackColor / 0x100 )  % 256 )  / 256.00, ( Int( nBackColor / 0x10000 ) % 256 ) / 256.00 )
   ENDIF

   IF Empty( nRound )

      HPDF_Page_Rectangle( ::hPage, nLeft, ::nHeight - nBottom, nRight - nLeft,  nBottom - nTop )

   ELSE

      nRay = Round( iif( ::nWidth > ::nHeight, Min( nRound,Int( ::nHeight / 2 ) ), Min( nRound,Int(::nWidth / 2 ) ) ), 0 )

      xposTop := ::nHeight - nTop
      xposBotton := ::nHeight - nBottom

      HPDF_Page_MoveTo ( ::hPage, nLeft + nRay,  xposTop )
      HPDF_Page_LineTo ( ::hPage, nRight - nRay, xposTop )

      HPDF_Page_CurveTo( ::hPage, nRight, xposTop, nRight,  xposTop, nRight,  xposTop - nRay )

      HPDF_Page_LineTo ( ::hPage, nRight, xposBotton + nRay )
      HPDF_Page_CurveTo( ::hPage, nRight, xposBotton, nRight, xposBotton, nRight - nRay,  xposBotton  )
      HPDF_Page_LineTo ( ::hPage, nLeft + nRay, xposBotton )
      HPDF_Page_CurveTo( ::hPage, nLeft, xposBotton,  nLeft, xposBotton, nLeft, xposBotton + nRay )

      HPDF_Page_LineTo ( ::hPage, nLeft, xposTop - nRay )
      HPDF_Page_CurveTo( ::hPage, nLeft, xposTop,  nLeft, xposTop, nLeft + nRay, xposTop )


   ENDIF

   IF HB_ISNUMERIC( nBackColor )
      HPDF_Page_FillStroke ( ::hPage )
   ELSE
      HPDF_Page_Stroke ( ::hPage )
   ENDIF
   HPDF_Page_GRestore(::hPage)

RETURN Self

METHOD SetPen( oPen, nColor )
   IF oPen != NIL
      IF ValType( oPen ) == 'N'
         HPDF_Page_SetLineWidth( ::hPage, oPen )
      ELSE
         HPDF_Page_SetLineWidth( ::hPage, oPen:nWidth )
         nColor:= oPen:nColor
      ENDIF
   ENDIF
   IF ValType( nColor ) == 'N'
      HPDF_Page_SetRGBStroke( ::hPage, ( nColor  % 256 ) / 256.00, ( Int( nColor / 0x100 )  % 256 ) / 256.00, ( Int( nColor / 0x10000 ) % 256 ) / 256.00 )
   ENDIF
RETURN Self

METHOD CmRoundBox( nTop, nLeft, nBottom, nRight, nWidth, nHeight, oPen, nColor, nBackColor, lFondo )
RETURN ::RoundBox( nTop * 72 / 2.54, nLeft * 72 / 2.54, nBottom * 72 / 2.54, nRight * 72 / 2.54, nWidth * 72 / 2.54, nHeight * 72 / 2.54, oPen, nColor, nBackColor, lFondo )

METHOD SayRotate( nTop, nLeft, cTxt, oFont, nClrText, nAngle )

   LOCAL aBackColor
   LOCAL nRadian := ( nAngle / 180 ) * 3.141592 /* Calcurate the radian value. */

    IF ValType( nClrText ) == 'N'
      aBackColor:= HPDF_Page_GetRGBFill( ::hPage )
      HPDF_Page_SetRGBFill( ::hPage, ( Int( nClrText / 0x10000 ) % 256 ) / 256.00, ( Int( nClrText / 0x100 )  % 256 )  / 256.00 , ( nClrText  % 256 ) / 256.00 )
   ENDIF

   /* FONT and SIZE*/
   If !Empty( oFont )
       HPDF_Page_SetFontAndSize( ::hPage, oFont[1], oFont[2] )
   EndI

   /* Rotating text */
   HPDF_Page_BeginText( ::hPage )
   HPDF_Page_SetTextMatrix( ::hPage, cos( nRadian ),;
                                     sin( nRadian ),;
                                     -( sin( nRadian ) ),;
                                     cos( nRadian ), nLeft, HPDF_Page_GetHeight( ::hPage )-( nTop ) )
   HPDF_Page_ShowText( ::hPage, cTxt )

   IF ValType( aBackColor ) == 'A'
      HPDF_Page_SetRGBFill( ::hPage, aBackColor[1], aBackColor[2], aBackColor[3] )
   ENDIF

   HPDF_Page_EndText( ::hPage )

Return NIL
